---
title: Home
menu: sidebar
weight: 1
---

## Cinc

A Free-as-in-Beer distribution of the open source software of Chef Software Inc. See [goals]({{< ref "goals.md" >}}) for details, or follow our [blog]({{< ref "blog/_index.md" >}}) for updates on the project.

The Cinc team is proud to present:

### Cinc Projects

- [Cinc Client]({{< relref "page/start/client.md" >}}), built from Chef Infra&trade;
- [Cinc Workstation]({{< relref "page/start/workstation.md" >}}), built from Chef Workstation&trade;
- [Cinc Auditor]({{< relref "page/start/auditor.md" >}}), built from Chef InSpec&trade;
- [Cinc Server]({{< ref "download#cinc-server" >}}), built from Chef Chef Infra Server&trade;

### Coming Soon

- [Cinc Packager]({{< ref "download#cinc-packager" >}}), built from Chef Habitat&trade;

Come chat with us! [community slack](http://community-slack.chef.io/) channel #community-distros
