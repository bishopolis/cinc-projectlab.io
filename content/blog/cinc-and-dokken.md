---
date: "2021-12-01T14:44:33+08:00"
title: "Cinc and Kitchen Dokken"
authors: ["dbeck"]
tags:
  - Cinc
  - Client
  - Kitchen
---

# Why use Kitchen Dokken?

In a word, *speed*. The kitchen-dokken plugin is designed to make testing your Cinc/Chef code as fast as possible. As you can probably determine from the name, the plugin uses containers to speed up the process, both for the OS being provisioned and for the Cinc/Chef client.

There are of course some trade-offs required to achieve this speed, firstly the plugin is Chef/Cinc specific (not really an issue if you are reading this!). Second, the OS containers are not like for like when compared to a full VM installation, so there may be some things you can't test properly in the container.

You can read more about the project here: [https://github.com/test-kitchen/kitchen-dokken](https://github.com/test-kitchen/kitchen-dokken)

# Configuring Kitchen Dokken for Cinc

Configuring Test Kitchen to work with Cinc was covered in the [Cooking with Cinc](../cooking_with_cinc/) post, here just the provisioner needed to updated to get it to work. For kitchen-dokken however, we will also need to update the driver as this references the client container to use.

The basic configuration for Dokken when using Chef is:

```yaml
driver:
  name: dokken
  chef_version: latest

transport:
  name: dokken

provisioner:
  name: dokken

verifier:
  name: inspec

platforms:
  - name: rockylinux-8
    driver:
      image: dokken/rockylinux-8

suites:
  - name: default
    run_list:
      - recipe[testing::default]
```

To change the client container that's used, we need to update both the image and the tag that are pulled. As an example to use the latest `17` version of the official Cinc container we would change the driver section to be:

```yaml
driver:
  name: dokken
  chef_image: cincproject/cinc
  chef_version: 17
```

Next we need to update the provisioner so that it knows how to run the Cinc client. This differs from the settings used in [Cooking with Cinc](..cooking_with_cinc) in that the client is not downloaded as part of the provisioner any more, that was handled by the driver in this case, so we just need to update the product and client binary names:

```yaml
provisioner:
  name: dokken
  product_name: cinc
  chef_binary: /opt/cinc/bin/cinc-client
```

And that's it! Our completed example now looks like this:

```yaml
driver:
  name: dokken
  chef_image: cincproject/cinc
  chef_version: 17

transport:
  name: dokken

provisioner:
  name: dokken
  product_name: cinc
  chef_binary: /opt/cinc/bin/cinc-client

verifier:
  name: inspec

platforms:
  - name: rockylinux-8
    driver:
      image: dokken/rockylinux-8

suites:
  - name: default
    run_list:
      - recipe[testing::default]
```

Running `kitchen converge` will pull any missing containers the first time it's run and from then on will be considerably faster.