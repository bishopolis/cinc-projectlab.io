---
date: "2022-04-20T14:07:31-0700"
title: "Cinc Server is now Stable"
authors: ["ramereth"]
tags:
  - Cinc
  - Server
---

Thanks to our various contributors, we are excited to announce that Cinc Server
14.14.1 is the first release that is now considered stable! You can find additional information on how to install and
use Cinc Server [here](/start/server).
